<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pabrik', function (Blueprint $table) {
            $table->id();
            $table->string('name')->change();
            if(Schema::hasTable('perusahaan')) {
                    $table->integer('perusahaan_id')->unsigned();
                    $table->foreign('perusahaan_id')->references('id')->on('perusahaan')->onDelete('cascade');
                }
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pabrik');
    }
}
