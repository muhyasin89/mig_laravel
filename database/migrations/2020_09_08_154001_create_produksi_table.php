<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProduksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produksi', function (Blueprint $table) {
            $table->id();
            $table->string('bulan');
            $table->string('tahun');
            $table->decimal('anggaran');
            $table->decimal('realisasi');
            $table->decimal('anggaran_tahunan');
            $table->decimal('anggaran_kg_ha');
            $table->decimal('realisasi_kg_ha');
            $table->decimal('anggaran_tahunan_kg_ha');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kebun');
    }
}
