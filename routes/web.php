<?php

use Illuminate\Support\Facades\Route;
use App\Perusahaan;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::resource('perusahaan', 'PerusahaanController');

Route::resource('pabrik', 'PabrikController');

Route::resource('produksi', 'LaporanController');

Route::resource('kebun', 'KebunController');
