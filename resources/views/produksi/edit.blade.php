@extends('layouts.admin')

@section('content')
 <div class="col-lg-11">

        {{ Html::ul($errors->all()) }}
            {!! Form::model($produksi, ['method' => 'PUT','route' => ['produksi.update', $produksi->id]]) !!}
            <div class="form-group">
                {{ Form::label('name', 'Name') }}
                {{ Form::text('name', $produksi->name, array('class' => 'form-control')) }}
            </div>
            {{ Form::submit('Edit Produksi!', array('class' => 'btn btn-primary')) }}
        {{ Form::close() }}
    </div>

@endsection