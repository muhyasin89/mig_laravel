@extends('layout')

@section('content')
    <div class="col-lg-11">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        {{ Html::ul($errors->all()) }}
        {{ Form::open(array('url' => 'produksi', 'files' => true)) }}
            <div class="form-group">
                {{ Form::label('bulan', 'Bulan') }}
                {{ Form::text('bulan', Request::old('bulan'), array('class' => 'form-control')) }}
            </div>
             <div class="form-group">
                {{ Form::label('tahun', 'Tahun') }}
                {{ Form::text('tahun', Request::old('tahun'), array('class' => 'form-control')) }}
            </div>
             <div class="form-group">
                {{ Form::label('anggaran', 'Anggaran') }}
                {{ Form::text('anggaran', Request::old('anggaran'), array('class' => 'form-control')) }}
            </div>
             <div class="form-group">
                {{ Form::label('realisasi', 'Realisasi') }}
                {{ Form::text('realisasi', Request::old('realisasi'), array('class' => 'form-control')) }}
            </div>
             <div class="form-group">
                {{ Form::label('anggaran_tahunan', 'Anggaran Tahunan') }}
                {{ Form::text('anggaran_tahunan', Request::old('anggaran_tahunan'), array('class' => 'form-control')) }}
            </div>
             <div class="form-group">
                {{ Form::label('anggaran_kg_ha', 'Anggaran KG Ha') }}
                {{ Form::text('anggaran_kg_ha', Request::old('anggaran_kg_ha'), array('class' => 'form-control')) }}
            </div>
             <div class="form-group">
                {{ Form::label('realisasi_kg_ha', 'Realisasi KG Ha') }}
                {{ Form::text('realisasi_kg_ha', Request::old('bulrealisasi_kg_haan'), array('class' => 'form-control')) }}
            </div>
             <div class="form-group">
                {{ Form::label('anggaran_tahunan_kg_ha', 'Anggaran Tahunan KG Ha') }}
                {{ Form::text('anggaran_tahunan_kg_ha', Request::old('anggaran_tahunan_kg_ha'), array('class' => 'form-control')) }}
            </div>
        
        {{ Form::submit('Create Produksi!', array('class' => 'btn btn-primary')) }}
        {{ Form::close() }}
    </div>

@endsection