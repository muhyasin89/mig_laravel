@extends('layout')
@section('content')
   
    <div class="container">
     <h3>Produksi</h3>
     <table class="table table-striped table-bordered right">
        <thead>
            <div class="row">
            <a href="{{ url('/produksi/create') }}"><button class="btn btn-primary">
                    Create Produksi
                </button></a>
            </div>
            <tr>
                <td>No</td>
                <td>Bulan</td>
                <td>Tahun</td>
                <td>Anggaran</td>
                <td>Realisasi</td>
                <td>Anggaran Tahunan</td>
                <td>Anggaran KG Ha</td>
                <td>Realisasi KG Ha</td>
                <td>Anggaran Tahunan KG Ha</td>
                <td>Action</td>
            </tr>
        </thead>
            <tbody>
                <?php $i =1; ?>
                @foreach($produksi as $key => $value)
                <tr>
                    <td><?php echo $i; $i++; ?></td>
                    <td>{{ $value->name }} </td>
                    <td>{{ $value->bulan }} </td>
                    <td>{{ $value->tahun }} </td>
                    <td>{{ $value->anggaran }} </td>
                    <td>{{ $value->realisasi }} </td>
                    <td>{{ $value->anggaran_tahunan }} </td>
                    <td>{{ $value->anggaran_kg_ha }} </td>
                    <td>{{ $value->realisasi_kg_ha }} </td>
                    <td>{{ $value->anggaran_tahunan_kg_ha }} </td>
                    <td>
                        <a href="{{ url('produksi/'.$value->id) }}"><button class="btn btn-info">Show</button> </a>
                        <a href="{{ url('produksi/'.$value->id.'/edit') }}"><button class="btn btn-warning">Edit</button></a>
                        {{Form::open([ 'method'  => 'DELETE', 'route' => [ 'projects.destroy', $value->id  ] ])}}
                        {{Form::button('<i class="fa fa-trash-o"></i> Delete', array('type' => 'submit', 'class' => 'btn btn-danger'))}}
                        {{ Form::close() }}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        </div>
@endsection