@extends('layout')

@section('content')
    <h3>Produksi</h3>
    <table class="table table-striped table-bordered">
        <div class="row">
             <a href="{{ url('/produksi/create') }}"><button class="btn btn-primary">
                        Create Produksi
                    </button></a>
        </div>
        <tr>
            <td>Tahun</td>
            <td>{{ $produksi->tahun }}</td>
        </tr>
        <tr>
            <td>Anggaran</td>
            <td>{{ $produksi->anggaran }}</td>
        </tr>
        <tr>
            <td>Realisasi</td>
            <td>{{ $produksi->realisasi }}</td>
        </tr>
        <tr>
            <td>Anggaran Tahunan</td>
            <td>{{ $produksi->anggaran_tahunan }}</td>
        </tr>
        <tr>
            <td>Anggaran KG Ha</td>
            <td>{{ $produksi->anggaran_kg_ha }}</td>
        </tr>
        <tr>
            <td>Realisasi KG Ha</td>
            <td>{{ $produksi->realisasi_kg_ha }}</td>
        </tr>
        <tr>
            <td>Anggaran Tahunan KG Ha</td>
            <td>{{ $produksi->anggaran_tahunan_kg_ha }}</td>
        </tr>
        <tr>
            <td>Action</td>
            <td> <a href="{{ url('produksi'.$produksi->id.'/edit') }}"><button class="btn btn-warning">
                        Edit
                    </button></a>
                {{Form::open([ 'method'  => 'DELETE', 'route' => [ 'produksi.delete', $produksi->id  ] ])}}
                {{Form::button('<i class="fa fa-trash-o"></i> Delete', array('type' => 'submit', 'class' => 'btn btn-danger'))}}
                {{ Form::close() }}</td>
        </tr>
    </table>
@endsection