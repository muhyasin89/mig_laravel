<!--/**
 * Created by PhpStorm.
 * User: muhyasin89
 * Date: 30/11/17
 * Time: 19.08
 */-->
@extends('layout')

@section('content')
    <div class="col-lg-11">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        {{ Html::ul($errors->all()) }}
        {{ Form::open(array('url' => 'perusahaan', 'files' => true)) }}
            <div class="form-group">
                {{ Form::label('name', 'Name') }}
                {{ Form::text('name', Request::old('name'), array('class' => 'form-control')) }}
            </div>
        
        {{ Form::submit('Create Perusahaan!', array('class' => 'btn btn-primary')) }}
        {{ Form::close() }}
    </div>

@endsection