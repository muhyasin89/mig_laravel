@extends('layouts.admin')

@section('content')
 <div class="col-lg-11">

        {{ Html::ul($errors->all()) }}
            {!! Form::model($perusahaan, ['method' => 'PUT','route' => ['perusahaan.update', $perusahaan->id]]) !!}
            <div class="form-group">
                {{ Form::label('name', 'Name') }}
                {{ Form::text('name', $perusahaan->name, array('class' => 'form-control')) }}
            </div>
            {{ Form::submit('Edit Perusahaan!', array('class' => 'btn btn-primary')) }}
        {{ Form::close() }}
    </div>

@endsection