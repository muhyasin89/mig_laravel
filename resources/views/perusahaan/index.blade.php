@extends('layout')
@section('content')
    <h3>Perusahaan</h3>
     <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <td colspan="5">
                <a href="{{ url('/perusahaan/create') }}"><button class="btn btn-primary">
                        Create Perusahaan
                    </button></a>
            </td>
        </tr>
        <tr>
            <td>No</td>
            <td>Name</td>
            <td>Action</td>
        </tr>
        </thead>
        <tbody>
         <?php $i =1; ?>
        @foreach($perusahaan as $key => $value)
        <tr>
            <td><?php echo $i; $i++; ?></td>
            <td>{{ $value->name }} </td>
            <td>
                <a href="{{ url('admin/projects/'.$value->id) }}"><button class="btn btn-info">Show</button> </a>
                <a href="{{ url('admin/projects/'.$value->id.'/edit') }}"><button class="btn btn-warning">Edit</button></a>
                {{Form::open([ 'method'  => 'DELETE', 'route' => [ 'projects.destroy', $value->id  ] ])}}
                {{Form::button('<i class="fa fa-trash-o"></i> Delete', array('type' => 'submit', 'class' => 'btn btn-danger'))}}
                {{ Form::close() }}
            </td>
        </tr>
        @endforeach

@endsection