@extends('layout')

@section('content')
    <h3>Perusahaan</h3>
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <td colspan="4">
                <a href="{{ url('/perusahaan/create') }}"><button class="btn btn-primary">
                        Create Perusahaan
                    </button></a>
            </td>
        </tr>
        <tr>
            <td>Name</td>
            <td>{{ $perusahaan->name }}</td>
        </tr>
        
        <tr>
            <td>Action</td>
            <td> <a href="{{ url('perusahaan'.$perusahaan->id.'/edit') }}"><button class="btn btn-warning">
                        Edit
                    </button></a>
                {{Form::open([ 'method'  => 'DELETE', 'route' => [ 'perusahaan.delete', $perusahaan->id  ] ])}}
                {{Form::button('<i class="fa fa-trash-o"></i> Delete', array('type' => 'submit', 'class' => 'btn btn-danger'))}}
                {{ Form::close() }}</td>
        </tr>
        </thead>
    </table>
@endsection