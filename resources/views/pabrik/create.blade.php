@extends('layout')

@section('content')
    <div class="col-lg-11">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        {{ Html::ul($errors->all()) }}
        {{ Form::open(array('url' => 'pabrik', 'files' => true)) }}
            <div class="form-group">
                {{ Form::label('name', 'Name') }}
                {{ Form::text('name', Request::old('name'), array('class' => 'form-control')) }}
            </div>
            <div class="form-group">
                {{ Form::label('perusahaan', 'Perusahaan') }}
                {{ Form::select('perusahaan', [$perusahaan]);}}
            </div>
        {{ Form::submit('Create Pabrik!', array('class' => 'btn btn-primary')) }}
        {{ Form::close() }}
    </div>

@endsection