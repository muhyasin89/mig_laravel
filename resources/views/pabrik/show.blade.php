@extends('layout')

@section('content')
    <h3>Pabrik</h3>
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <td colspan="4">
                <a href="{{ url('/pabrik/create') }}"><button class="btn btn-primary">
                        Create Pabrik
                    </button></a>
            </td>
        </tr>
        <tr>
            <td>Name</td>
            <td>{{ $pabrik->name }}</td>
        </tr>
        
        <tr>
            <td>Action</td>
            <td> <a href="{{ url('pabrik'.$pabrik->id.'/edit') }}"><button class="btn btn-warning">
                        Edit
                    </button></a>
                {{Form::open([ 'method'  => 'DELETE', 'route' => [ 'pabrik.delete', $pabrik->id  ] ])}}
                {{Form::button('<i class="fa fa-trash-o"></i> Delete', array('type' => 'submit', 'class' => 'btn btn-danger'))}}
                {{ Form::close() }}</td>
        </tr>
        </thead>
    </table>
@endsection