@extends('layouts.admin')

@section('content')
 <div class="col-lg-11">

        {{ Html::ul($errors->all()) }}
            {!! Form::model($pabrik, ['method' => 'PUT','route' => ['pabrik.update', $pabrik->id]]) !!}
            <div class="form-group">
                {{ Form::label('name', 'Name') }}
                {{ Form::text('name', $pabrik->name, array('class' => 'form-control')) }}
            </div>
            {{ Form::submit('Edit Pabrik!', array('class' => 'btn btn-primary')) }}
        {{ Form::close() }}
    </div>

@endsection