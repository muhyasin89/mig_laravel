@extends('layout')
@section('content')
    <h3>Pabrik</h3>
     <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <td colspan="5">
                <a href="{{ url('/pabrik/create') }}"><button class="btn btn-primary">
                        Create Pabrik
                    </button></a>
            </td>
        </tr>
        <tr>
            <td>No</td>
            <td>Name</td>
            <td>Perusahaan</td>
            <td>Action</td>
        </tr>
        </thead>
        <tbody>
         <?php $i =1; ?>
        @foreach($pabrik as $key => $value)
        <tr>
            <td><?php echo $i; $i++; ?></td>
            <td>{{ $value->name }} </td>
            @if($value->perusahaan)
                <td>{{ $value->perusahaan->name }} </td>
            @else
                <td> - </td>
            @endif
            <td>
                <a href="{{ url('pabrik/'.$value->id) }}"><button class="btn btn-info">Show</button> </a>
                <a href="{{ url('pabrik/'.$value->id.'/edit') }}"><button class="btn btn-warning">Edit</button></a>
                {{Form::open([ 'method'  => 'DELETE', 'route' => [ 'pabrik.destroy', $value->id  ] ])}}
                {{Form::button('<i class="fa fa-trash-o"></i> Delete', array('type' => 'submit', 'class' => 'btn btn-danger'))}}
                {{ Form::close() }}
            </td>
        </tr>
        @endforeach

@endsection