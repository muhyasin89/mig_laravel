<!--/**
 * Created by PhpStorm.
 * User: muhyasin89
 * Date: 30/11/17
 * Time: 19.08
 */-->
@extends('layout')

@section('content')
    <div class="col-lg-11">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        {{ Html::ul($errors->all()) }}
        {{ Form::open(array('url' => 'kebun', ['method' => 'PUT','route' => ['kebun.update', $kebun->id]])) }}
            <div class="form-group">
                {{ Form::label('name', 'Name') }}
                {{ Form::text('name', Request::old('name'), array('class' => 'form-control')) }}
            </div>
            <div class="form-group">
                {{ Form::label('devisi', 'Devisi') }}
                {{ Form::text('devisi', Request::old('devisi'), array('class' => 'form-control')) }}
            </div>
            <div class="form-group">
                {{ Form::label('perusahaan', 'Perusahaan') }}
                {{{ Form::select('perusahaan', $perusahaan, null, ['class' => 'form-control']) }}
            </div>
            <div class="form-group">
                {{ Form::label('block', 'Block') }}
                {{ Form::text('block', Request::old('block'), array('class' => 'form-control')) }}
            </div>
            <div class="form-group">
                {{ Form::label('tahun_tanam', 'Tahun Tanam') }}
                {{ Form::text('tahun_tanam', Request::old('tahun_tanam'), array('class' => 'form-control')) }}
            </div>
        
        {{ Form::submit('Edit Kebun!', array('class' => 'btn btn-primary')) }}
        {{ Form::close() }}
    </div>

@endsection