@extends('layout')

@section('content')
    <h3>Kebun</h3>
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <td colspan="6">
                <a href="{{ url('/kebun/create') }}"><button class="btn btn-primary">
                        Create Kebun
                    </button></a>
            </td>
        </tr>
        <tr>
            <td>Name</td>
            <td>{{ $kebun->name }}</td>
        </tr>
         <tr>
            <td>Devisi</td>
            <td>{{ $kebun->devisi }}</td>
        </tr>
         <tr>
            <td>Name</td>
            <td>
            @if($kebun->perusahaan)
                {{ $kebun->perusahaan->name }}
            @else
                tidak ada perusahaan
            @endif
            </td>
        </tr>
         <tr>
            <td>Block</td>
            <td>{{ $kebun->block }}</td>
        </tr>
        <tr>
            <td>Tahun Tanam</td>
            <td>{{ $kebun->tahun_tanam }}</td>
        </tr>
        <tr>
            <td>Action</td>
            <td> <a href="{{ url('kebun'.$kebun->id.'/edit') }}"><button class="btn btn-warning">
                        Edit
                    </button></a>
                {{Form::open([ 'method'  => 'DELETE', 'route' => [ 'kebun.delete', $kebun->id  ] ])}}
                {{Form::button('<i class="fa fa-trash-o"></i> Delete', array('type' => 'submit', 'class' => 'btn btn-danger'))}}
                {{ Form::close() }}</td>
        </tr>
        </thead>
    </table>
@endsection