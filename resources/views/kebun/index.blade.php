@extends('layout')
@section('content')
    <h3>Kebun</h3>
     <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <td colspan="7">
                <a href="{{ url('/kebun/create') }}"><button class="btn btn-primary">
                        Create Kebun
                    </button></a>
            </td>
        </tr>
        <tr>
            <td>No</td>
            <td>Name</td>
            <td>Devisi</td>
            <td>Perusahaan</td>
            <td>Block</td>
            <td>Tahun Tanam</td>
            <td>Action</td>
        </tr>
        </thead>
        <tbody>
         <?php $i =1; ?>
        @foreach($kebun as $key => $value)
        <tr>
            <td><?php echo $i; $i++; ?></td>
            <td>{{ $value->name }} </td>
            <td>{{ $value->devisi }} </td>
            
            <td>
            @if($value->perusahaan)
            {{ $value->perusahaan->name }} 
            @else
                <p>Tidak ada perusahaan</p>
            @endif
            </td>
            
            <td>{{ $value->devisi }} </td>
            <td>{{ $value->block }} </td>
            <td>{{ $value->tahun_tanam }} </td>
            <td>
                <a href="{{ url('admin/projects/'.$value->id) }}"><button class="btn btn-info">Show</button> </a>
                <a href="{{ url('admin/projects/'.$value->id.'/edit') }}"><button class="btn btn-warning">Edit</button></a>
                {{Form::open([ 'method'  => 'DELETE', 'route' => [ 'projects.destroy', $value->id  ] ])}}
                {{Form::button('<i class="fa fa-trash-o"></i> Delete', array('type' => 'submit', 'class' => 'btn btn-danger'))}}
                {{ Form::close() }}
            </td>
        </tr>
        @endforeach

@endsection