<?php

namespace App\Http\Controllers;

use App\Pabrik;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PabrikController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pabrik = Pabrik::all();

        return view('pabrik.index')->with(compact('pabrik'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $perusahaan =  collect(DB::table('perusahaan')->get())->keyBy('id');
        return view('pabrik.create')->with('perusahaan', $perusahaan);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate($this->rules());

        $pabrik = new Pabrik;
        $pabrik->name = $request->name;

        if($pabrik->save()){
            return redirect('perusahaan/');
        }else{
            $validator->errors()->add('database', 'Something is wrong with this database cannot input record!');
            return back()
                ->withErrors($validator)
                ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pabrik = Pabrik::find($id);

        return view('pabrik.show')->with('pabrik',$pabrik);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pabrik = Pabrik::find($id);
        $perusahaan =  collect(DB::table('perusahaan')->get())->keyBy('id');

        return view('pabrik.edit')->with('pabrik',$pabrik)->with('perusahaan', $perusahaan);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         request()->validate($this->rules());

        $pabrik = Pabrik::find($id);

        if($pabrik->save()){
            return redirect('pabrik/');
        }else{
            $validator->errors()->add('database', 'Something is wrong with this database cannot input record!');
            return back()
                ->withErrors($validator)
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pabrik = Pabrik::find($id);

        if($pabrik != null){
            $pabrik->delete();
            Session::flash('message', 'Successfully deleted the Perusahaan!');
            return Redirect::to('perusahaan');
        }

        return back()->with(['message'=> 'Object is null, wrong id']);
    }

    public function rules(){
        return [
            'name'=> 'required',
            'perusahaan'=> 'required'
        ];
    }
}
