<?php

namespace App\Http\Controllers;

use App\Perusahaan;
use Illuminate\Http\Request;


class PerusahaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perusahaan = Perusahaan::all();

        return view('perusahaan.index')->with(compact('perusahaan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('perusahaan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate($this->rules());

        $perusahaan = new Perusahaan;
        $perusahaan->name = $request->name;

        if($perusahaan->save()){
            return redirect('perusahaan/');
        }else{
            $validator->errors()->add('database', 'Something is wrong with this database cannot input record!');
            return back()
                ->withErrors($validator)
                ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $perusahaan = Perusahaan::find($id);

        return view('perusahaan.show')->with('perusahaan',$perusahaan);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $perusahaan = Perusahaan::find($id);

        return view('perusahaan.edit')->with('perusahaan',$perusahaan);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate($this->rules());

        $perusahaan = Perusahaan::find($id);

        if($perusahaan->save()){
            return redirect('perusahaan/');
        }else{
            $validator->errors()->add('database', 'Something is wrong with this database cannot input record!');
            return back()
                ->withErrors($validator)
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $perusahaan = Perusahaan::find($id);

        if($perusahaan != null){
            $perusahaan->delete();
            Session::flash('message', 'Successfully deleted the Perusahaan!');
            return Redirect::to('perusahaan');
        }

        return back()->with(['message'=> 'Object is null, wrong id']);
    }


    public function rules(){
        return [
            'name'=> 'required',
        ];
    }
}
