<?php

namespace App\Http\Controllers;

use App\Kebun;
use App\Perusahaan;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KebunController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kebun = Kebun::all();

        return view('kebun.index')->with(compact('kebun'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $perusahaan =  collect(DB::table('perusahaan')->get())->keyBy('id');
        return view('kebun.create')->with('perusahaan', $perusahaan);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate($this->rules());

        $kebun = new Kebun;
        $kebun->name = $request->name;
        $kebun->devisi = $request->devisi;
        $kebun->perusahaan_id = $request->perusahaan_id;
        $kebun->block = $request->block;
        $kebun->tahun_tanam = $request->tahun_tanam;

        if($kebun->save()){
            return redirect('kebun/');
        }else{
            $validator->errors()->add('database', 'Something is wrong with this database cannot input record!');
            return back()
                ->withErrors($validator)
                ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kebun = Kebun::find($id);

        return view('perusahaan.show')->with('kebun',$kebun);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $perusahaan =  collect(DB::table('perusahaan')->get())->keyBy('id');
        return view('kebun.edit')->with('perusahaan',$perusahaan);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate($this->rules());

        $kebun = Kebun::find($id);

        if($kebun->save()){
            return redirect('kebun/');
        }else{
            $validator->errors()->add('database', 'Something is wrong with this database cannot input record!');
            return back()
                ->withErrors($validator)
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kebun = Kebun::find($id);

        if($kebun != null){
            $kebun->delete();
            Session::flash('message', 'Successfully deleted the Kebun!');
            return Redirect::to('kebun');
        }

        return back()->with(['message'=> 'Object is null, wrong id']);
    }

     public function rules(){
        return [
            'name'=> 'required',
            'devisi'=> 'required', 
            'perusahaan'=> 'required|exists:perusahaan', 
            'devisi'=> 'required',
            'block'=> 'required',
            'tahun_tanam'=> 'required'
        ];
    }
}
