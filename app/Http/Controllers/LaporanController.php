<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produksi;


class LaporanController extends Controller
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $produksi = Produksi::all();

        return view('produksi.index')->with(compact('produksi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('produksi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate($this->rules());

        $produksi = new Produksi;
        $produksi->name = $request->name;

        if($produksi->save()){
            return redirect('perusahaan/');
        }else{
            $validator->errors()->add('database', 'Something is wrong with this database cannot input record!');
            return back()
                ->withErrors($validator)
                ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $produksi = Produksi::find($id);

        return view('produksi.show')->with('produksi',$produksi);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produksi = Produksi::find($id);

        return view('produksi.edit')->with('produksi',$produksi);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate($this->rules());

        $produksi = Produksi::find($id);

        if($produksi->save()){
            return redirect('produksi/');
        }else{
            $validator->errors()->add('database', 'Something is wrong with this database cannot input record!');
            return back()
                ->withErrors($validator)
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produksi = Perusahaan::find($id);

        if($produksi != null){
            $produksi->delete();
            Session::flash('message', 'Successfully deleted the produksi!');
            return Redirect::to('produksi');
        }

        return back()->with(['message'=> 'Object is null, wrong id']);
    }
}
