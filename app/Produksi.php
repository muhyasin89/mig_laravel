<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produksi extends Model
{
    protected $table = 'produksi';
    protected $fillable = ['id', 'bulan', 'tahun', 'anggaran', 'realisasi', 'anggaran_tahunan','anggaran_kg_ha', 'realisasi_kg_ha', 'anggaran_tahunan_kg_ha'];
}
