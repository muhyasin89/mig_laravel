<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kebun extends Model
{
    protected $table = 'kebun';
    protected $fillable = ['name', 'devisi', 'perusahaan_id', 'devisi', 'block', 'tahun_tanam'];
}
