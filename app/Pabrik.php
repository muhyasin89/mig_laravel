<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pabrik extends Model
{
    protected $table = 'pabrik';
    protected $fillable = ['name', 'perusahaan'];
}
